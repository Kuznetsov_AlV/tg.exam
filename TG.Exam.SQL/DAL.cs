﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace TG.Exam.SQL
{
    public class DAL
    {
        private const string GetAllOrdersQueryTemplate = 
            "SELECT OrderId, OrderCustomerId, OrderDate FROM dbo.Orders";

        private const string GetAllOrdersWithCustomersQueryTemplate =
            "SELECT O.OrderId, O.OrderCustomerId, O.OrderDate, C.CustomerFirstName, C.CustomerLastName " +
            "FROM Orders AS O " +
            "JOIN Customers AS C ON C.CustomerId = O.OrderCustomerId";

        private const string GetAllOrdersWithPriceUnderQueryTemlate =
            "SELECT O.OrderId, O.OrderCustomerId, O.OrderDate " +
            "FROM Orders AS O " +
            "JOIN OrdersItems AS OI ON O.OrderId = OI.OrderId " +
            "JOIN Items AS I ON OI.ItemId = I.ItemId " +
            "WHERE I.ItemPrice > {0}" +
            "GROUP BY O.OrderId, O.OrderCustomerId, O.OrderDate";

        private const string DeleteCustomerQueryTemplate =
            "DELETE C " +
            "FROM Customers AS C " +
            "JOIN Orders AS O ON C.CustomerId = O.OrderCustomerId " +
            "WHERE O.OrderId = {0}";

        private const string GetAllItemsAndTheirOrdersCountIncludingTheItemsWithoutOrdersQueryTemplate =
            "SELECT I.ItemId, I.ItemName, SUM(IsNull(OI.Count, 0)) AS Count " +
            "FROM Items AS I " +
            "LEFT JOIN OrdersItems AS OI ON I.ItemId = OI.ItemId " +
            "GROUP BY I.ItemId, I.ItemName";

        private SqlConnection GetConnection() 
        {
            var connectionString = ConfigurationManager.AppSettings["ConnectionString"];
            var connection = new SqlConnection(connectionString);
            connection.Open();
            return connection;
        }

        private DataSet GetData(string sql)
        {
            var dataSet = new DataSet();
            using (var connection = GetConnection())
            {
                using (var command = new SqlCommand(sql, connection))
                {
                    using (var adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dataSet);
                    }
                }
            }
            return dataSet;
        }

        private void Execute(string sql)
        {
            using (var connection = GetConnection())
            {
                using (var command = new SqlCommand(sql, connection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        public DataTable GetAllOrders()
        {
            var sql = GetAllOrdersQueryTemplate;
            var dataSet = GetData(sql);
            var result = dataSet.Tables.OfType<DataTable>().FirstOrDefault();
            return result;
        }
        public DataTable GetAllOrdersWithCustomers()
        {
            var sql = GetAllOrdersWithCustomersQueryTemplate;
            var dataSet = GetData(sql);
            var result = dataSet.Tables.OfType<DataTable>().FirstOrDefault();
            return result;
        }

        public DataTable GetAllOrdersWithPriceUnder(int price)
        {
            var sql = string.Format(GetAllOrdersWithPriceUnderQueryTemlate, price);
            var dataSet = GetData(sql);
            var result = dataSet.Tables.OfType<DataTable>().FirstOrDefault();
            return result;
        }

        public void DeleteCustomer(int orderId)
        {
            var sql = string.Format(DeleteCustomerQueryTemplate, orderId);
            Execute(sql);
        }

        internal DataTable GetAllItemsAndTheirOrdersCountIncludingTheItemsWithoutOrders()
        {
            var sql = GetAllItemsAndTheirOrdersCountIncludingTheItemsWithoutOrdersQueryTemplate;
            var dataSet = GetData(sql);
            var result = dataSet.Tables.OfType<DataTable>().FirstOrDefault();
            return result;
        }
    }
}