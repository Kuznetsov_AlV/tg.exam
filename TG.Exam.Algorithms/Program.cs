﻿using System;

namespace TG.Exam.Algorithms
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Foo result: {0}", Foo(7, 2, 8));
            Console.WriteLine("Bar result: {0}", string.Join(", ", Bar(new[] { 7, 2, 8 })));
            Console.ReadKey();
        }

        // The algorithm for calculation particular number from fibonacci sequence.
        // But it's possible to pass to this method parameters different from the standard ones for the fibonacci sequence.
        // Advantages:
        // - this's quite simple implementation;
        // - complexity O(n).
        // Disadvantages:
        // - it's possible to get a StackOverflowException, due to the use of recursion;
        // - this isn't the fastest implementation.
        public static int Foo(int a, int b, int c)
        {
            if (1 < c)
                return Foo(b, b + a, c - 1);
            else
                return a;
        }

        // The algorithm for calculation particular number from fibonacci sequence without recursion.
        // Advantages:
        // - this's very simple implementation;
        // - complexity O(n);
        // Disadvantages:
        // - this isn't the fastest implementation.
        public static int FooNew(int a, int b, int c)
        {
            for (int i = 1; i < c; i++)
            {
                b += a;
                a = b - a;
            }
            return a; 
        }

        // This is the "Bubble sort" algorithm.
        // Advantage: 
        // - very simple code. 
        // Disadvantage:
        // - complexity O(n^2). It means this algorithm works very slowly with long arrays.
        public static int[] Bar(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
                for (int j = 0; j < arr.Length - 1; j++)
                    if (arr[j] > arr[j + 1])
                    {
                        int t = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = t;
                    }
            return arr;
        }

        // This is "Quick sort" algorithm.
        // Advantage: 
        // - the best and average complexity is O(nlogn). 
        // So, this algorithm works much faster (please, see "AlgorithmsTests").
        // Disadvantages: 
        // - the worst complexity is O(n^2);
        // - a bit more complicated code.
        public static int[] BarNew(int[] arr)
        {
            QuickSort(arr, 0, arr.Length - 1);
            return arr;
        }

        private static void QuickSort(int[] arr, int startIndex, int endIndex)
        {
            if (startIndex >= endIndex)
                return;

            var pivotIndex = Partition(arr, startIndex, endIndex);
            QuickSort(arr, startIndex, pivotIndex - 1);
            QuickSort(arr, pivotIndex + 1, endIndex);
        }

        private static int Partition(int[] arr, int startIndex, int endIndex)
        {
            var pivot = startIndex - 1;
            for (var i = startIndex; i < endIndex; i++)
                if (arr[i] < arr[endIndex])
                    Swap(ref arr[++pivot], ref arr[i]);
            Swap(ref arr[++pivot], ref arr[endIndex]);
            return pivot;
        }

        private static void Swap(ref int firstItem, ref int secondItems)
        {
            var temp = firstItem;
            firstItem = secondItems;
            secondItems = temp;
        }
    }
}
