﻿using System.Collections.Generic;
using System.Web.Http;

namespace Salestech.Exam.WebMVC.Controllers
{
    public class UserApiController : ApiController
    {
        public IEnumerable<TG.Exam.WebMVC.Models.User> Get()
        {
            return TG.Exam.WebMVC.Models.User.GetAll();
        }
    }
}