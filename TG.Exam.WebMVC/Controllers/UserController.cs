﻿using System.Web.Mvc;

namespace Salestech.Exam.WebMVC.Controllers
{
    public class UserController : Controller
    {
        // GET: Users
        public ViewResult List()
        {
            return View(TG.Exam.WebMVC.Models.User.GetAll());
        }
    }
}