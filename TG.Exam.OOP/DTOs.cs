﻿namespace TG.Exam.OOP
{
    public abstract class EntityBase
    {
        public abstract string ToString2();
    }

    public class Employee : EntityBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Salary { get; set; }

        public override string ToString2() => 
            $"{nameof(FirstName)}: {FirstName}, {nameof(LastName)}: {LastName}, {nameof(Salary)}: {Salary}";
    }

    public class SalesManager : Employee
    {
        public int BonusPerSale { get; set; }
        public int SalesThisMonth { get; set; }

        public override string ToString2() => 
            $"{base.ToString2()}, {nameof(BonusPerSale)}: {BonusPerSale}, {nameof(SalesThisMonth)}: {SalesThisMonth}";
    }

    public class CustomerServiceAgent : Employee
    {
        public int Customers { get; set; }

        public override string ToString2() => 
            $"{base.ToString2()}, {nameof(Customers)}: {Customers}";
    }

    public class Dog : EntityBase
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public override string ToString2() => $"{nameof(Name)}: {Name}, {nameof(Age)}: {Age}";
    }
}