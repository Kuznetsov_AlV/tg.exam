﻿using System;
using System.Collections.Concurrent;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using log4net;
using log4net.Config;

namespace TG.Exam.Refactoring
{
    // Issues:
    // 1. The specified connectionString may not exist. In this case a NullReferenceException will be thrown.
    // Need to check if the connection string exist.
    // 2. The cache doesn't make sense, because this one is initialized after every call. 
    // That's why locks don't make sense too.
    // Need to make this one a static property and use "ConcurrentDictinary" instead of "Dictionary" 
    // or even better to create separate type.
    // It's also required to remember about the process of cache invalidation.
    // 3. It's possible to pass any string as a "OrderId". It may lead to some sql injections. 
    // Moreover, it makes no sence to pass "orderId" as string, because this one is stored in the database table as integer.
    // Need to use "int" instead of "string". 
    // 4. SqlConnection, SqlCommand and SqlDataReader are "IDisposable" objects, but they aren't closed after using.
    // Need to use "using".
    // 5. The part of the code about stopping watch and writing log is repeated 3 times.
    // Need to move this part to the "finally" part.
    // 6. Getting fields from reader by indexes looks unsafe. The fields in the query can be swapped.
    // Of course, using raw sql-query in the code may lead to many other issues.
    // Need to get fields by names.
    // 7. If the reader returns null, explicit cast operator will throw a NullReferenceException. 
    // Need to use "as" operator with null checking.
    // Maybe it would be better to throw an exception, but I don't have enough information about expected behaviour.
    // 8. The method "LoadOrder" has many responsibilities.
    // Need to split this one. 
    // 9. Strings in the code looks dirty.
    // Need to move all strings to constants.
    public class OrderService : IOrderService
    {
        private const string ConnectionStringName = "OrdersDBConnectionString";
        private const string ElapsedTimeMessage = "Elapsed - {0}";
        private const string ErrorMessage = "An error has occurred. Please contact your administrator.";
        private const string ConfigurationErrorMessage = "The specified connectionString doesn't exist";
        private const string QueryTemplate =
            "SELECT OrderId, OrderCustomerId, OrderDate FROM Orders WHERE OrderId={0}";

        private static readonly ILog logger = LogManager.GetLogger(typeof(OrderService));
        private static readonly ConcurrentDictionary<int, Order> cache = new ConcurrentDictionary<int, Order>();

        private readonly string connectionString;

        public OrderService()
        {
            BasicConfigurator.Configure();
            this.connectionString = ConfigurationManager.ConnectionStrings[ConnectionStringName]?.ConnectionString
                ?? throw new ConfigurationErrorsException(ConfigurationErrorMessage);
        }

        public Order LoadOrder(int orderId)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            try
            {
                return this.GetOrder(orderId);
            }
            catch (SqlException ex)
            {
                logger.Error(ex.Message, ex);
                throw new ApplicationException(ErrorMessage, ex);
            }
            finally
            {
                stopWatch.Stop();
                logger.InfoFormat(ElapsedTimeMessage, stopWatch.Elapsed);
            }
        }

        private Order GetOrder(int orderId)
        {
            if (cache.ContainsKey(orderId) && cache.TryGetValue(orderId, out var cachedOrder))
                return cachedOrder;

            var order = this.GetOrderFromDb(orderId);
            if (order != null)
                cache.TryAdd(orderId, order);

            return order;
        }

        private Order GetOrderFromDb(int orderId)
        {
            using (var connection = new SqlConnection(this.connectionString))
            {
                var query = string.Format(QueryTemplate, orderId);
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                            return null;

                        return new Order
                        {
                            OrderId = reader[nameof(Order.OrderId)] as int? ?? 0,
                            OrderCustomerId = reader[nameof(Order.OrderCustomerId)] as int? ?? 0,
                            OrderDate = reader[nameof(Order.OrderDate)] as DateTime? ?? DateTime.MinValue
                        };
                    }
                }
            }
        }
    }
}