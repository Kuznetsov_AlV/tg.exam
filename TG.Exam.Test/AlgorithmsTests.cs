﻿using System;
using System.Diagnostics;
using TG.Exam.Algorithms;
using Xunit;

namespace TG.Exam.Test
{
    public class AlgorithmsTests
    {
        private const int LongNumbersSetlength = 10000;

        [Theory]
        [InlineData(0, 1, 1)]
        [InlineData(7, 2, 8)]
        public void BothFooMethods_SameInput_SameResult(int a, int b, int c)
        {
            var fooResult = Program.Foo(a, b, c);
            var fooNewResult = Program.FooNew(a, b, c);
            Assert.Equal(fooResult, fooNewResult);
        }

        [Theory]
        [InlineData(new int[0])]
        [InlineData(new[] { 7, 2, 8 })]
        public void BothBarMethods_SameNumbersSet_SameResult(int[] arr)
        {
            var barResult = Program.Bar(arr);
            var barNewResult = Program.BarNew(arr);
            Assert.Equal(barResult, barNewResult);
        }

        [Fact]
        public void BarNew_LongNumbersSet_WorksFaster()
        {
            var random = new Random(DateTime.Now.Millisecond);
            var barTestArray = new int[LongNumbersSetlength];
            var barNewTestArray = new int[LongNumbersSetlength];
            for (int i = 0; i < LongNumbersSetlength; i++)
            {
                var number = random.Next(1, LongNumbersSetlength);
                barTestArray[i] = number;
                barNewTestArray[i] = number;
            }

            var barTime = GetMeasuredTime(barTestArray, Program.Bar);
            var barNewTime = GetMeasuredTime(barNewTestArray, Program.BarNew);
            Assert.True(barNewTime < barTime);
        }

        private long GetMeasuredTime(int[] testArray, Func<int[], int[]> measuredMethod)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var result = measuredMethod(testArray);
            stopWatch.Stop();
            return stopWatch.ElapsedMilliseconds;
        }
    }
}